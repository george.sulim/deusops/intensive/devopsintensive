# Используем образ Node.js для сборки приложения
FROM node:14-alpine AS build
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
# Запускаем приложение при запуске контейнера
CMD ["npm", "start"]
